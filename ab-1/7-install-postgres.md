# Install Postgres

Install the library

```bash
sudo apt install postgresql postgresql-contrib
sudo systemctl start postgresql.service
```

Create a new user

```bash
sudo -u postgres createuser --interactive
```

