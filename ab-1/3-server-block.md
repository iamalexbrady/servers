See /templates/nginx/server-blocks for example server blocks including:
* Serve a directory of static files: static-files.txt
* Proxy the request to a webserver listening on a port: proxy-to-port.txt