# Setup Domain Routing

## Installing Nginx

```bash
sudo apt update
sudo apt install nginx
sudo ufw allow 'Nginx Full'
```

## Configuring Server Blocks

```bash
sudo mkdir -p /var/www/alexbrady.co/html
sudo chown -R $USER:$USER /var/www/alexbrady.co/html
sudo chmod -R 755 /var/www/alexbrady.co
sudo echo "<html><head><title>site</title></head><body>This site is working</body></html>" >> /var/www/alexbrady.co/html/index.html
sudo nano /etc/nginx/sites-available/alexbrady.co
```

Paste into /etc/nginx/sites-available/alexbrady.co:
```
server {
        listen 80;
        listen [::]:80;
    	# listen 443 ssl;
    	# listen [::]:443 ssl;

        root /var/www/alexbrady.co/html;
        index index.html index.htm index.nginx-debian.html;

        server_name alexbrady.co www.alexbrady.co;

        location / {
                try_files $uri $uri/ =404;
        }
}
```

```bash
sudo ln -s /etc/nginx/sites-available/alexbrady.co /etc/nginx/sites-enabled/
sudo nano /etc/nginx/nginx.conf;
# In /etc/nginx/nginx.conf, uncomment `server_names_hash_bucket_size 64;`
sudo systemctl restart nginx
```

### Setting up a new app?
**Do not re-run these exact instructions on ab-1.** If you are reviewing these documents in order to set up a new website, replace every instance of `alexbrady.co` with the new domain you are setting up.

## DNS

On the DNS provider, e.g. [NameCheap.com](https://namecheap.com), set up the following records:
* Type: A-record, Host: @, value: 159.223.202.182
* Type: URL redirect, Host: www, value: https://alexbrady.co

### Setting up a new app?

The values of the A-record needs to be the IP address of the Digital Ocean droplet. This is the same host IP used to SSH onto the server. Another way to get this value is to issue the command `curl -4 icanhazip.com`.


## Reference Documents
* [Install NGINX on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04)
