# Install pm2

```bash
npm install -g npm@9.1.1
npm install -g pm2
```

# pm2 Usage

```bash
# Start the application in daemon mode
pm2 start [script.ext] # for example: pm2 start index.js --name=AppName

# List apps being managed by pm2
pm2 list

# Stop an app
pm2 stop [AppName] # for example: pm2 stop AppName

# Restart an app
pm2 restart [AppName]
```