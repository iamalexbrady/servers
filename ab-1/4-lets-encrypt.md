# Certbot and SSL

## Actual Run

```bash
sudo apt install certbot python3-certbot-nginx
sudo certbot --nginx -d alexbrady.co

## Output:
## Certificate is saved at: /etc/letsencrypt/live/alexbrady.co/fullchain.pem
## Key is saved at:         /etc/letsencrypt/live/alexbrady.co/privkey.pem
```

While previously a manual process, the `certbot` program now automatically renews the certificate. Use the following commands to see the cron timer that is setup by the program and test the command the timer invokes.

```bash
sudo systemctl status certbot.timer
sudo certbot renew --dry-run
```

## Setting up a new app/domain?

If you are configuring a new domain name, do not use the above command verbatim. Instead, substitute the appropriate values specific to your use case into the following template:

```bash
sudo apt install certbot python3-certbot-nginx
sudo certbot --nginx -d [domain1.com] -d [www.domain1.com]
```

## Reference Documents
* [Nginx and Let's Encrypt on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04)
