# Install Node

Install nvm. Use nvm to install node.

```bash
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
nvm install 18
nvm alias default 18
sudo apt-get install build-essential
``` 
