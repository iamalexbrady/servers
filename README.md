# README

This repo is an explanation of servers setup on [Digital Ocean](https://digitalocean.com).

All servers go through Digital Ocean's suggested walkthroughs:

* [Initial Server Setup Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-20-04)
* [Install NGINX on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04)
* [Nginx and Let's Encrypt on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04)

Each server on DO has its own directory in this repository. The within each server's directory are the pertinent files and scripts located/run on the server instance.

**Important:** Keep this repository up to date! When at all possible, make any changes in this repo locally, then git commit/push and pull the changes to the remote server from source control, and only run invoke changes that have already been saved to source. Scripts and dependencies *can be* run directly on the server instance, but do diligence to ensure changes are recorded in this repository and saved to source control.

---

## Active Servers
* ab-1