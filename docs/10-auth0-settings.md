# Auth0 Settings

The Node services use Auth0 for user authentication. Upon having a full environment stack set up, a service will have 3 Auth0 applications: one for local development, one for a staging environment, one for the production environment. The app is configured with Auth0 within the .env file.

With the Auth0 dashboard, each applicaiton's settings need to be set with Allowed Callback URLs, Allowed Logout URLs. These values have some implication within the Service and are standardarized, the same across the different apps.

```yml
# Allowed Callback URLs
local: https://localhost:3001/auth0/callback
staging: https://stg.[domain]/auth0/callback
prod: https://[domain]/auth0/callback

# Allowed Logout URLs
local: https://localhost:3001
staging: https://stg.[domain]
prod: https://[domain]
```

The routing of the domain and subdomains is handled at the DNS provider level, NameCheap.com.