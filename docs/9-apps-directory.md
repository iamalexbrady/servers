# Create an apps directory

The apps directory is priopriatary location for where
in the filesystem application files are stored.

The standard procedure for creating this file on new linux instances will be as follows.

```bash
# as root user
# it is important to be root user

$ sudo mkdir /apps
$ sudo chown :sudo /apps
$ sudo chmod 775 /apps

# This should create a directory, /apps
# with a user owner of root and a group owner of sudo
# The file permissions are rwx for user, rwx for group and r-x for other
```

For a user to make changes to the apps directory, they must be added to the sudo group. To add a user to the sudo group, run the command:

```
$ sudo usermod -a -G sudo [USERNAME]
```

Feel free to improve this area of file management.