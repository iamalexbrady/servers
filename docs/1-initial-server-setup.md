# Initial Server Setup

## Actual Run

```bash
adduser pop
usermod -aG sudo pop
ufw allow OpenSSH
ufw enable
```

## Setting up a new server/user?

Do not re-run the above command verbatim. Instead, substitute the appropriate values into the following commands:

```bash
adduser [user]
usermod -aG sudo [user]
ufw allow OpenSSH
ufw enable
```

## Reference Documents
[Initial Server Setup Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-20-04)