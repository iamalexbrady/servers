# Node services

Patterns for running our standardized node services.

The services are start in production using the command `npm run start`. This invokes pm2 with the correct flags and whatnot. This API for application start should remain the same and standardized. If specialty functions need to be invoked at start, an approach that can be taken is to have `npm run start` invoke a bash script which eventually invokes pm2.

### Bootstrapping
```bash
npm install --production
```

### Start Scripts
```bash
# Start prod
npm run start

# Start dev
npm run dev
```

### Database
To date, our node applications manage their databases with sequelize, and utilize Postgres as the database server.

The application specific database user must first be created in Postgres. See the [Install Postgres] documentation for finer details. Commands using `npm` and `npx` must be run from the application's root directory. Commands using `npm run` are defined in package.json.

```bash
# Create the application user
sudo -u postgres createuser --interactive --pwprompt

# Create a database, this should be performed once when the application is first installed and not repeated
npx sequelize db:create --env=production

# Migrate the database with NODE_ENV=production. This is a common task
npm run db:migrate:prod

# Seed the database with NODE_ENV=production. This is a common task
npm run db:seed:prod
```