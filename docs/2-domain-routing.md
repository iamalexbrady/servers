# Setup Domain Routing

## Installing Nginx

```bash
sudo apt update
sudo apt install nginx
sudo ufw allow 'Nginx Full'
```

## Configuring Server Blocks

#### Setting up a new app?
**Do not re-run these exact instructions on ab-1.** If you are reviewing these documents in order to set up a new website, replace every instance of `alexbrady.co` with the new domain you are setting up.

This page covers two options for configuring NGINX. Any one service, site or app will likely only need one approach. An application server which listens on a ports, e.g. `rails s` or `http.listen()`, will be configured to received requests proxied form NGINX to the port it's listening on. A static website, HTML/CSS/JS, will require a static file directory.

### Proxy to Port
Use this guide to accept all traffic to DOMAIN and SUBDOMAIN.DOMAIN at the NGINX server and proxy the request to an application server such as puma or node.

The use of DOMAIN should be replaced with the domain name an enduser will type into a navigation bar. Example: alexbrady.co

The use of SUBDOMAIN.DOMAIN is similar, but covers the case of having pertinent subdomains such as api.alexbrady.co, www.alexbrady.co, etc, where api and www are the example subdomains.

This script is a TEMPLATE and the variable names must be replaced for it to work. The variable names are in ALL CAPS and at the time of the documentation writing are DOMAIN, SUBDOMAIN.DOMAIN, and APP_PORT.

```bash
sudo nano /etc/nginx/sites-available/DOMAIN
```

Paste into /etc/nginx/sites-available/DOMAIN:
```
server {
    listen 80;
    listen [::]:80;

    server_name [DOMAIN];

    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_pass http://127.0.0.1:[APP PORT]; # Replace APP PORT with actual port
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }
}
```

### Serve static files from a directory
The following example sets up files located at /var/www/alexbrady.co/html to be served via NGINX.

```bash
sudo mkdir -p /var/www/alexbrady.co/html
sudo chown -R $USER:$USER /var/www/alexbrady.co/html
sudo chmod -R 755 /var/www/alexbrady.co
sudo echo "<html><head><title>site</title></head><body>This site is working</body></html>" >> /var/www/alexbrady.co/html/index.html
sudo nano /etc/nginx/sites-available/alexbrady.co
```

Paste into /etc/nginx/sites-available/alexbrady.co:
```
server {
        listen 80;
        listen [::]:80;

        root /var/www/alexbrady.co/html;
        index index.html index.htm index.nginx-debian.html;

        server_name alexbrady.co www.alexbrady.co;

        location / {
                try_files $uri $uri/ =404;
        }
}
```

### Enable NGINX with the new site
Regarless of whther static files or a server is used, finally, let the NGINX process know to begin listening for site traffic. Before this step, we have created static configuration. By adding the configuration to NGINXs "enabled" list, the web server will begin to handle requests the configuration is set to receive.

```bash
# Create a symlink to sites-available within sites-enabled
sudo ln -s /etc/nginx/sites-available/alexbrady.co /etc/nginx/sites-enabled/

# In /etc/nginx/nginx.conf, uncomment `server_names_hash_bucket_size 64;`
# This is suggested by the Digital Ocean walkthroughs
sudo nano /etc/nginx/nginx.conf

# Restart nginx to pick up the changes
sudo systemctl restart nginx
```

## DNS

On the DNS provider, e.g. [NameCheap.com](https://namecheap.com), set up the following records:
* Type: A-record, Host: @, value: 159.223.202.182
* Type: URL redirect, Host: www, value: https://alexbrady.co

The values of the A-record needs to be the IP address of the Digital Ocean droplet. This is the same host IP used to SSH onto the server. Another way to get this value is to issue the command `curl -4 icanhazip.com`.

DNS propogation can take anywhere from 5 to 30 minutes. **Test the completion of propogation by accessing the server at http://DOMAIN** If you get a Bad Gateway error, you have successfully accepted the request via NGINX. Make sure the application server is running, and preferably using the PM2 process manager. (See the PM2 docs for running the application server with PM2.)

## Accepting SSL at NGINX
We use certbot to manage SSL certificates. Follow the certbot process for generating the SSL certificates, verifying domain ownership, and setting up the certificate refresh cronjob.

```bash
# Use certbot to generate Nginx SSL configuration
sudo certbot --nginx -d [DOMAIN]

# certbot overwrites the listen directives within the NGINX configuration. To check:
cat /etc/nginx/sites-available/[DOMAIN]
```

## Reference Documents
* [Install NGINX on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04)
