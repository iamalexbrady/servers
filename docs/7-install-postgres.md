# Install Postgres

Install the library

```bash
sudo apt install postgresql postgresql-contrib
sudo systemctl start postgresql.service
```

Create a new user

```bash
sudo -u postgres createuser --interactive --pwprompt
```

Create a postgres user your Ubuntu user as well as one for any application that manages its own database e.g. through Sequelize or Rails.

Recommended configuration for a person user:
```yml
Name: Exact name of unix user (required)
Password: User preference
Be a super user: y
Create new databases: y
Create more new roles: y
```

Recommended configuration for an application user:
```yml
Name: Exact same name as the application's environment variable
Password: Same as application env variable
Be a superuser: n
Create new databases: y (required yes)
Create more new roles: n
```