# Install nginx

https://adamtheautomator.com/nginx-on-mac/

https://gist.github.com/jimothyGator/5436538

sudo cp -r ssl-certs /usr/local/etc/nginx

server {
	listen 80;
	listen [::]:80;
	listen [::]:443 ssl ipv6only=on;
	listen 443 ssl;

    # this is different than linux
	ssl_certificate /usr/local/etc/nginx/ssl-certs/empyrehq.local.crt;
    ssl_certificate_key /usr/local/etc/nginx/ssl-certs/empyrehq.local.key;

	server_name empyrehq.local;

	location / {
		proxy_set_header X-Forwarded-For 
$proxy_add_x_forwarded_for;
		proxy_set_header Host $host;
		proxy_pass http://127.0.0.1:3000;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
	}
}

