#!/bin/bash

# Create a Locally Signed SSL Certificate for use in Local Development
# To use: ./make-cert.bash [DOMAIN]
# This will generate files [DOMAIN].crt and [DOMAIN].key

# This file should be run before the install-ca-cert.bash script

DOMAIN=$1

echo "Generating ${DOMAIN}.key and ${DOMAIN}.crt"
openssl req -x509 -out ${DOMAIN}.crt -keyout ${DOMAIN}.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj "/CN=${DOMAIN}" -extensions EXT -config <( \
   printf "[dn]\nCN=${DOMAIN}\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:${DOMAIN}\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

