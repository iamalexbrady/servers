#!/bin/bash

# To be used in conjunction with make-cert.bash
# Given the output files DOMAIN.crt and DOMAIN.key,
# Put them where NGINX can use them
# To use: ./install-ca-cert.bash [DOMAIN]

DOMAIN=$1

# The location to copy both files is coupled with the nginx conf files for the domain
# The exact same location is used within the nginx conf's ssl_certificate and ssl_certificate_key directives
# (/etc/nginx/sites-available/(DOMAIN))
echo "\nCopying ${DOMAIN}.key and ${DOMAIN}.crt to /etc/nginx/ssl-certs (requires sudo)"
cp ${DOMAIN}.key ${DOMAIN}.crt /etc/nginx/ssl-certs

# This step in the process installs the certificate in the trust store
# https://ubuntu.com/server/docs/security-trust-store
if ! [ -x "$(command -v update-ca-certificates)" ]; then
  echo "\nca-certificates not installed. Installing..."
  sudo apt-get install -y ca-certificates
fi
if [[ ! -e /usr/local/share/ca-certificates ]]; then
    mkdir /usr/local/share/ca-certificates
fi

echo "\nCopying ${DOMAIN}.crt to /usr/local/share/ca-certificates"
sudo cp ${DOMAIN}.crt /usr/local/share/ca-certificates
sudo update-ca-certificates
echo "\nWhen first loading ${DOMAIN} in a browser, you may need to accept security risk and continue. The prompt should not recur."

echo "\nDone."