#!/bin/bash

# This script installs nvm, uses nvm to install node v18
# Makes node v18 the sustem default
# Installs build-essential to the system

curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
. ~/.bashrc
nvm install 18
nvm alias default 18
sudo apt-get install -y build-essential