#!/bin/bash

# creates a user with the given username
# adds user to sudo-ers group
# To use: ./create-sudo-user.sh [username]

adduser $1
usermod -aG sudo $1