server {
    # The listen directives will be overwritten by certbot if certbot
    # is used for issuing SSL certificates.
    listen 80;
    listen [::]:80;
    # listen 443 ssl;
    # listen [::]:443 ssl;

    # The file directory listed after root must contain the website files
    root /var/www/[DOMAIN.TLD]/html;
    index index.html index.htm index.nginx-debian.html;

    server_name [DOMAIN.TLD];

    location / {
        try_files $uri $uri/ =404;
    }
}